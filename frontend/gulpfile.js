var gulp = require('gulp');
    uglify = require('gulp-uglify');
    rename = require('gulp-rename');
    sass = require('gulp-sass');
    browserSync = require('browser-sync');
    reload = browserSync.reload;


//// SCRIPTS 
gulp.task('scripts', function() {
	gulp.src(['app/js/**/*.js', '!app/js/**/*.min.js'])
		.pipe(rename({suffix:'.min'}))
		.pipe(uglify())
		.pipe(gulp.dest('app/js'))
		.pipe(reload({stream:true}));
});

//// SASS 
gulp.task('sass', function(){
	gulp.src('app/scss/style.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('app/css/'))
	.pipe(reload({stream:true}))
});

//// HTML
gulp.task('html', function(){
	gulp.src('app/**/*.html')
		.pipe(reload({stream:true}));
});

//// BROWSER SYNC
gulp.task('browser-sync', function(){
	browserSync({
		server:{
			baseDir: "./app/"
		}
	})
});

//// WATCH 
gulp.task('watch', function(){
	gulp.watch('app/js/**/*.js', ['scripts']);
	gulp.watch('app/scss/**/*.scss', ['sass']);
	gulp.watch('app/**/*.html', ['html']);
});


gulp.task('default', ['scripts', 'sass', 'html', 'browser-sync', 'watch']);